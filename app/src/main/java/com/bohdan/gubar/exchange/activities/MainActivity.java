package com.bohdan.gubar.exchange.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.bohdan.gubar.exchange.R;
import com.bohdan.gubar.exchange.fragments.MainFragment;

public class MainActivity extends AppCompatActivity {

    public static final String USD_CODE = "USD";
    public static final String EUR_CODE = "EUR";
    public static final String RUB_CODE = "RUB";

    public static final String BUNDLE_CURRENCY = "CURR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            MainFragment fragment = new MainFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.activity_main, fragment);
            transaction.commit();
        }
        //setAlarmTime();

    }


}
