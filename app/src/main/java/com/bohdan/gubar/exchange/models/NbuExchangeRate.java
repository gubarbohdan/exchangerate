package com.bohdan.gubar.exchange.models;

public class NbuExchangeRate {

    private Integer r030;
    private String txt;
    private Double rate;
    private String cc;
    private String exchangedate;

    /**
     * @return The r030
     */
    public Integer getR030() {
        return r030;
    }

    /**
     * @param r030 The r030
     */
    public void setR030(Integer r030) {
        this.r030 = r030;
    }

    /**
     * @return The txt
     */
    public String getTxt() {
        return txt;
    }

    /**
     * @param txt The txt
     */
    public void setTxt(String txt) {
        this.txt = txt;
    }

    /**
     * @return The rate
     */
    public Double getRate() {
        return rate;
    }

    /**
     * @param rate The rate
     */
    public void setRate(Double rate) {
        this.rate = rate;
    }

    /**
     * @return The cc
     */
    public String getCc() {
        return cc;
    }

    /**
     * @param cc The cc
     */
    public void setCc(String cc) {
        this.cc = cc;
    }

    /**
     * @return The exchangedate
     */
    public String getExchangedate() {
        return exchangedate;
    }

    /**
     * @param exchangedate The exchangedate
     */
    public void setExchangedate(String exchangedate) {
        this.exchangedate = exchangedate;
    }

}