package com.bohdan.gubar.exchange.fragments;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bohdan.gubar.exchange.R;
import com.bohdan.gubar.exchange.activities.MainActivity;
import com.bohdan.gubar.exchange.models.NbuExchangeRate;
import com.bohdan.gubar.exchange.models.PbExchangeRate;
import com.bohdan.gubar.exchange.rest.ApiClient;
import com.bohdan.gubar.exchange.rest.ApiInterfaceNbu;
import com.bohdan.gubar.exchange.rest.ApiInterfacePb;
import com.bohdan.gubar.exchange.services.UpdateService;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    @BindView(R.id.twUsdRate)
    TextView twUsdRate;
    @BindView(R.id.twEurRate)
    TextView twEurRate;
    @BindView(R.id.twRubRate)
    TextView twRubRate;
    @BindView(R.id.twDate)
    TextView twDate;

    ApiInterfaceNbu apiInterfaceNbu;
    ApiInterfacePb apiInterfacePb;
    Observable<List<NbuExchangeRate>> nbuExchangeRateObservable;
    Observable<List<PbExchangeRate>> pbExchangeRateObservable;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        Calendar today = Calendar.getInstance();
        String date = today.get(Calendar.DAY_OF_MONTH) + "." + (today.get(Calendar.MONTH) + 1) + "." + today.get(Calendar.YEAR);

        twDate.setText(String.format(getResources().getString(R.string.lbl_date), date));

        apiInterfaceNbu = ApiClient.getClientNbu();

        setNbuExchangeRate();

        return view;
    }

    private void setNbuExchangeRate() {
        nbuExchangeRateObservable = apiInterfaceNbu.getNbuExchangeRate();
        nbuExchangeRateObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(list -> Observable.from(list))
                .subscribe(nbuExchangeRate -> {
                            switch (nbuExchangeRate.getCc()) {
                                case MainActivity.USD_CODE:
                                    twUsdRate.setText(nbuExchangeRate.getRate().toString());
                                    break;
                                case MainActivity.EUR_CODE:
                                    twEurRate.setText(nbuExchangeRate.getRate().toString());
                                    break;
                                case MainActivity.RUB_CODE:
                                    twRubRate.setText(nbuExchangeRate.getRate().toString());
                                    break;
                            }
                        },
                        error -> {
                            Log.e("Error NBU", "No Internet");
                            Toast.makeText(getContext(), R.string.msg_no_internet, Toast.LENGTH_SHORT).show();
                        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_archive_rate:
                showArchiveRate();
                break;
            case R.id.menu_graphic:
                showChart();
                break;
            case R.id.menu_time:
                setUpdateTime();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        ArchiveFragment archiveFragment = new ArchiveFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        String date = dayOfMonth + "." + (monthOfYear + 1) + "." + year;
        Bundle bundle = new Bundle();
        bundle.putString("DATE", date);
        archiveFragment.setArguments(bundle);
        getFragmentManager().popBackStack();
        transaction.replace(R.id.activity_main, archiveFragment);
        transaction.addToBackStack("Test");
        transaction.commit();
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        AlarmManager alarmMgr = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getActivity(), UpdateService.class);
        intent.setAction("Updating intent");
        PendingIntent alarmIntent = PendingIntent.getService(getActivity(), 0, intent, 0);


        // Set the alarm to start at approximately 2:00 p.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);

// With setInexactRepeating(), you have to use one of the AlarmManager interval
// constants--in this case, AlarmManager.INTERVAL_DAY.
        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, alarmIntent);
    }

    private void showArchiveRate() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = MyDatePicker.newInstance(
                MainFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    private void setUpdateTime() {
        Calendar now = Calendar.getInstance();

        TimePickerDialog tpd = TimePickerDialog.newInstance(MainFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true);
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    private void showChart() {
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.BUNDLE_CURRENCY, MainActivity.USD_CODE);
        ChartFragment chartFragment = new ChartFragment();
        chartFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_main, chartFragment);
        transaction.addToBackStack("Test");
        transaction.commit();
    }

    public static class MyDatePicker extends DatePickerDialog {
        @Override
        public boolean isOutOfRange(int year, int month, int day) {
            return year < 2015;
        }
    }
}
