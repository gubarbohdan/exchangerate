package com.bohdan.gubar.exchange.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bohdan.gubar.exchange.R;
import com.bohdan.gubar.exchange.activities.MainActivity;
import com.bohdan.gubar.exchange.models.ExchangeArchive;
import com.bohdan.gubar.exchange.rest.ApiClient;
import com.bohdan.gubar.exchange.rest.ApiInterfacePb;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArchiveFragment extends Fragment {

    @BindView(R.id.twArchiveDate)
    TextView twArchiveDate;

    @BindView(R.id.twUsdNbuRate)
    TextView twUsdNbuRate;
    @BindView(R.id.twUsdPbSaleRate)
    TextView twUsdPbSaleRate;
    @BindView(R.id.twUsdPbBuyRate)
    TextView twUsdPbBuyRate;

    @BindView(R.id.twEurNbuRate)
    TextView twEurNbuRate;
    @BindView(R.id.twEurPbSaleRate)
    TextView twEurPbSaleRate;
    @BindView(R.id.twEurPbBuyRate)
    TextView twEurPbBuyRate;

    @BindView(R.id.twRubNbuRate)
    TextView twRubNbuRate;
    @BindView(R.id.twRubPbSaleRate)
    TextView twRubPbSaleRate;
    @BindView(R.id.twRubPbBuyRate)
    TextView twRubPbBuyRate;


    ApiInterfacePb apiInterfacePb;
    Observable<ExchangeArchive> pbExchangeRateObservable;

    public ArchiveFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_archive, container, false);
        ButterKnife.bind(this, view);

        apiInterfacePb = ApiClient.getClientPb();
        String date = getArguments().getString("DATE");

        setPbExchangeRate(date);

        twArchiveDate.setText(String.format(getResources().getString(R.string.err_no_data), date));


        return view;
    }

    private void setPbExchangeRate(String date) {
        pbExchangeRateObservable = apiInterfacePb.getArchiveRateForDate(date);
        pbExchangeRateObservable.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .flatMap(list -> Observable.from(list.getExchangeRate()))
                .subscribe(pbExchangeRate -> {
                            twArchiveDate.setText(String.format(getResources().getString(R.string.lbl_date), date));
                            switch (pbExchangeRate.getCurrency()) {
                                case MainActivity.USD_CODE:
                                    twUsdNbuRate.setText(pbExchangeRate.getPurchaseRateNB().toString());
                                    twUsdPbBuyRate.setText(pbExchangeRate.getPurchaseRate().toString());
                                    twUsdPbSaleRate.setText(pbExchangeRate.getSaleRate().toString());
                                    break;
                                case MainActivity.EUR_CODE:
                                    twEurNbuRate.setText(pbExchangeRate.getPurchaseRateNB().toString());
                                    twEurPbBuyRate.setText(pbExchangeRate.getPurchaseRate().toString());
                                    twEurPbSaleRate.setText(pbExchangeRate.getSaleRate().toString());
                                    break;
                                case MainActivity.RUB_CODE:
                                    twRubNbuRate.setText(pbExchangeRate.getPurchaseRateNB().toString());
                                    twRubPbBuyRate.setText(pbExchangeRate.getPurchaseRate().toString());
                                    twRubPbSaleRate.setText(pbExchangeRate.getSaleRate().toString());
                                    break;
                            }

                        },
                        error -> {
                            Log.e("ERROR PB", "TEST");
                            Toast.makeText(getContext(), R.string.msg_no_internet, Toast.LENGTH_SHORT).show();
                        });
    }

}
