package com.bohdan.gubar.exchange.services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import com.bohdan.gubar.exchange.R;
import com.bohdan.gubar.exchange.activities.MainActivity;
import com.bohdan.gubar.exchange.models.NbuExchangeRate;
import com.bohdan.gubar.exchange.rest.ApiClient;
import com.bohdan.gubar.exchange.rest.ApiInterfaceNbu;

import java.util.List;

import rx.Observable;

public class UpdateService extends Service {

    ApiInterfaceNbu apiInterfaceNbu;
    Observable<List<NbuExchangeRate>> nbuExchangeRateObservable;

    StringBuilder stringBuilder;

    public UpdateService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        stringBuilder = new StringBuilder();
        apiInterfaceNbu = ApiClient.getClientNbu();

        Intent myintent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, myintent, 0);

        Notification.Builder mBuilder =
                new Notification.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pIntent)
                        .setContentTitle(getResources().getString(R.string.lbl_notif_title))
                        .setDefaults(Notification.DEFAULT_ALL);


        setNbuExchangeRate(mBuilder);


        return super.onStartCommand(intent, flags, startId);
    }

    private void setNbuExchangeRate(Notification.Builder builder) {
        nbuExchangeRateObservable = apiInterfaceNbu.getNbuExchangeRate();
        nbuExchangeRateObservable
                .flatMap(list -> Observable.from(list))
                .filter(item -> item.getCc().equals(MainActivity.USD_CODE) || item.getCc().equals(MainActivity.EUR_CODE))
                .subscribe(nbuExchangeRate -> {
                    switch (nbuExchangeRate.getCc()) {
                        case MainActivity.USD_CODE:
                            stringBuilder.append(MainActivity.USD_CODE + ": " + nbuExchangeRate.getRate().toString() + " ");
                            break;
                        case MainActivity.EUR_CODE:
                            stringBuilder.append(MainActivity.EUR_CODE + ": " + nbuExchangeRate.getRate().toString() + " ");
                            break;
//                case "RUB": twRubRate.setText(nbuExchangeRate.getRate().toString()); break;
                    }
                }, error -> {
                    Log.e("Error NBU", "TEST");
                    AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    Intent intent = new Intent(this, UpdateService.class);
                    intent.setAction("Error intent");
                    PendingIntent alarmIntent = PendingIntent.getService(this, 0, intent, 0);


                    alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                            SystemClock.elapsedRealtime() +
                                    60 * 1000 * 10, alarmIntent);
                }, () -> {
                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    builder.setContentText(stringBuilder.toString());
// mId allows you to update the notification later on.
                    mNotificationManager.notify(0, builder.build());
                    UpdateService.this.stopSelf();
                });

    }
}
