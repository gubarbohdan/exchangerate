package com.bohdan.gubar.exchange.rest;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by gubar on 23.11.2016.
 */

public class ApiClient {
    public static final String BASE_URL_PB = "https://api.privatbank.ua/p24api/";
    public static final String BASE_URL_NBU = "https://bank.gov.ua/NBUStatService/v1/statdirectory/";
    static RxJavaCallAdapterFactory factory = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
    static GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create();
    private static ApiInterfacePb apiInterfacePb;
    private static ApiInterfaceNbu apiInterfaceNbu;

    public static ApiInterfacePb getClientPb() {
        if (apiInterfacePb == null) {
            apiInterfacePb = new Retrofit.Builder()
                    .baseUrl(BASE_URL_PB)
                    .addConverterFactory(gsonConverterFactory)
                    .addCallAdapterFactory(factory)
                    .build().create(ApiInterfacePb.class);
        }
        return apiInterfacePb;
    }

    public static ApiInterfaceNbu getClientNbu() {
        if (apiInterfaceNbu == null) {
            apiInterfaceNbu = new Retrofit.Builder()
                    .baseUrl(BASE_URL_NBU)
                    .addConverterFactory(gsonConverterFactory)
                    .addCallAdapterFactory(factory)
                    .build().create(ApiInterfaceNbu.class);
        }
        return apiInterfaceNbu;
    }
}
