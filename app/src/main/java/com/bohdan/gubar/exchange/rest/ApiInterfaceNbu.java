package com.bohdan.gubar.exchange.rest;

import com.bohdan.gubar.exchange.models.NbuExchangeRate;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by gubar on 23.11.2016.
 */

public interface ApiInterfaceNbu {

    @GET("exchange?json")
    Observable<List<NbuExchangeRate>> getNbuExchangeRate();
}
