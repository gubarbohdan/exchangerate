
package com.bohdan.gubar.exchange.models;

public class ExchangeRate {

    private String baseCurrency;
    private String currency;
    private Double saleRateNB;
    private Double purchaseRateNB;
    private Double saleRate;
    private Double purchaseRate;

    /**
     * @return The baseCurrency
     */
    public String getBaseCurrency() {
        return baseCurrency;
    }

    /**
     * @param baseCurrency The baseCurrency
     */
    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    /**
     * @return The currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency The currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return The saleRateNB
     */
    public Double getSaleRateNB() {
        return saleRateNB;
    }

    /**
     * @param saleRateNB The saleRateNB
     */
    public void setSaleRateNB(Double saleRateNB) {
        this.saleRateNB = saleRateNB;
    }

    /**
     * @return The purchaseRateNB
     */
    public Double getPurchaseRateNB() {
        return purchaseRateNB;
    }

    /**
     * @param purchaseRateNB The purchaseRateNB
     */
    public void setPurchaseRateNB(Double purchaseRateNB) {
        this.purchaseRateNB = purchaseRateNB;
    }

    /**
     * @return The saleRate
     */
    public Double getSaleRate() {
        return saleRate;
    }

    /**
     * @param saleRate The saleRate
     */
    public void setSaleRate(Double saleRate) {
        this.saleRate = saleRate;
    }

    /**
     * @return The purchaseRate
     */
    public Double getPurchaseRate() {
        return purchaseRate;
    }

    /**
     * @param purchaseRate The purchaseRate
     */
    public void setPurchaseRate(Double purchaseRate) {
        this.purchaseRate = purchaseRate;
    }

}
