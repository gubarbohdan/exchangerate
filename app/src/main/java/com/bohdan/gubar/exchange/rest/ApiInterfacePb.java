package com.bohdan.gubar.exchange.rest;

import com.bohdan.gubar.exchange.models.ExchangeArchive;
import com.bohdan.gubar.exchange.models.PbExchangeRate;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by gubar on 23.11.2016.
 */

public interface ApiInterfacePb {

    @GET("pubinfo?json&exchange&coursid=3")
    Observable<List<PbExchangeRate>> getPbExchangeRate();

    @GET("exchange_rates?json")
    Observable<ExchangeArchive> getArchiveRateForDate(@Query("date") String date);
}
