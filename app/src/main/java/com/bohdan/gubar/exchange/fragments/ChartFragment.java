package com.bohdan.gubar.exchange.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bohdan.gubar.exchange.R;
import com.bohdan.gubar.exchange.activities.MainActivity;
import com.bohdan.gubar.exchange.models.ExchangeArchive;
import com.bohdan.gubar.exchange.rest.ApiClient;
import com.bohdan.gubar.exchange.rest.ApiInterfacePb;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.eazegraph.lib.charts.ValueLineChart;
import org.eazegraph.lib.models.ValueLinePoint;
import org.eazegraph.lib.models.ValueLineSeries;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChartFragment extends Fragment {


    @BindView(R.id.twGraphicTitle)
    TextView twTitle;

    ApiInterfacePb apiInterfacePb;
    Observable<ExchangeArchive> exchangeArchiveObservable;

    @BindView(R.id.cubiclinechart)
    ValueLineChart lineChart;

    @BindView(R.id.progressBar)
    ProgressWheel progressBar;

    @BindView(R.id.btnUsd)
    Button btnUsd;

    @BindView(R.id.btnEur)
    Button btnEur;

    @BindView(R.id.btnRub)
    Button btnRub;
    String currency;
    private ValueLineSeries series;
    private Calendar today;

    public ChartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chart, container, false);
        ButterKnife.bind(this, view);


        apiInterfacePb = ApiClient.getClientPb();

        series = new ValueLineSeries();
        series.setColor(0xFF56B7F1);

        currency = getArguments().getString(MainActivity.BUNDLE_CURRENCY);

        today = Calendar.getInstance();
        //today.roll(Calendar.DAY_OF_MONTH, -4);

        Calendar calendar = Calendar.getInstance();
        calendar.roll(Calendar.MONTH, false);

        String fromDate = calendar.get(Calendar.DAY_OF_MONTH) + "." + (calendar.get(Calendar.MONTH) + 1) + "." + calendar.get(Calendar.YEAR);
        String toDate = today.get(Calendar.DAY_OF_MONTH) + "." + (today.get(Calendar.MONTH) + 1) + "." + today.get(Calendar.YEAR);

        twTitle.setText(String.format(getResources().getString(R.string.lbl_graphic_title), currency, fromDate, toDate));

        setSeries(calendar);

        btnEur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString(MainActivity.BUNDLE_CURRENCY, MainActivity.EUR_CODE);
                ChartFragment chartFragment = new ChartFragment();
                chartFragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.activity_main, chartFragment);
                getFragmentManager().popBackStack();
                transaction.addToBackStack("Test");
                transaction.commit();
            }
        });

        btnUsd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString(MainActivity.BUNDLE_CURRENCY, MainActivity.USD_CODE);
                ChartFragment chartFragment = new ChartFragment();
                chartFragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.activity_main, chartFragment);
                getFragmentManager().popBackStack();
                transaction.addToBackStack("Test");
                transaction.commit();
            }
        });

        btnRub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString(MainActivity.BUNDLE_CURRENCY, MainActivity.RUB_CODE);
                ChartFragment chartFragment = new ChartFragment();
                chartFragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.activity_main, chartFragment);
                getFragmentManager().popBackStack();
                transaction.addToBackStack("Test");
                transaction.commit();
            }
        });

        return view;
    }

    private void setSeries(Calendar calendar) {
        if (calendar.get(Calendar.MONTH) != today.get(Calendar.MONTH) || calendar.get(Calendar.DAY_OF_MONTH) != today.get(Calendar.DAY_OF_MONTH)) {
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH) + 1;
            int year = calendar.get(Calendar.YEAR);

            exchangeArchiveObservable = apiInterfacePb.getArchiveRateForDate(day + "." + month + "." + year);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            exchangeArchiveObservable.subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter(list -> !list.getExchangeRate().isEmpty())
                    .flatMap(list -> Observable.from(list.getExchangeRate()))
                    .subscribe(rate -> {
                        if (rate.getCurrency().equals(currency)) {
                            series.addPoint(new ValueLinePoint(calendar.get(Calendar.DAY_OF_MONTH) + "." + calendar.get(Calendar.MONTH),
                                    rate.getPurchaseRate().floatValue() * 100));
                        }
                    }, error -> {
                        Toast.makeText(getContext(), R.string.msg_no_internet, Toast.LENGTH_SHORT).show();
                    }, () -> {
                        setSeries(calendar);
                    });
        } else {
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH) + 1;
            int year = calendar.get(Calendar.YEAR);

            exchangeArchiveObservable = apiInterfacePb.getArchiveRateForDate(day + "." + month + "." + year);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            exchangeArchiveObservable.subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter(list -> !list.getExchangeRate().isEmpty())
                    .flatMap(list -> Observable.from(list.getExchangeRate()))
                    .subscribe(rate -> {
                        if (rate.getCurrency().equals(currency)) {
                            series.addPoint(new ValueLinePoint(calendar.get(Calendar.DAY_OF_MONTH) + "." + calendar.get(Calendar.MONTH),
                                    rate.getPurchaseRate().floatValue() * 100));
                        }

                    }, error -> {
                        Toast.makeText(getContext(), R.string.msg_no_internet, Toast.LENGTH_SHORT).show();
                    }, () -> {
                        progressBar.setVisibility(View.GONE);
                        lineChart.addSeries(series);
                        lineChart.startAnimation();
                    });
        }

    }

}
